//importing problem2 function
let problem2Function = require("../problem2");

//executing the problem2 function
problem2Function((error, message) => {
  if (error) {
    console.log(error);
  } else {
    console.log(message);
  }
});
