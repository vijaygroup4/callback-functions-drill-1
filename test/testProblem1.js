//importing createFilesAndDeleteFiles Function
const createFilesAndDeleteFiles = require("../problem1");

let directoryPath = "./randomFolder";
let noOfFiles = 5;

//executing the createFilesAndDeleteFiles function
createFilesAndDeleteFiles(directoryPath, noOfFiles, (error, message) => {
  if (error) {
    console.log(error);
  } else {
    console.log(message);
  }
});
