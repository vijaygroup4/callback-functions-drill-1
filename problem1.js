//importing fs and path modules
const fs = require("fs");
const path = require("path");

//function that create random json files and can delete random files
function createFilesAndDeleteFiles(directoryPath, noOfFiles, callback) {
  //creating directory
  fs.mkdir(directoryPath, (error) => {
    if (error) {
      return callback(error);
    }

    let count = 1;
    //creating random json files
    for (let index = 1; index <= noOfFiles; index++) {
      let fileName = `file${index}.json`;
      let filePath = path.join(directoryPath, fileName);
      //generating random data
      let randomData = {
        data: Math.random(),
      };
      //writing that random data to random files
      fs.writeFile(filePath, JSON.stringify(randomData), (internalError) => {
        if (internalError) {
          return callback(internalError);
        }

        // after all iterations deleting the files that I have created
        if (count === noOfFiles) {
          callback(null, "successfully created the files");
          deleteFiles(directoryPath, noOfFiles, callback);
        }
        count += 1;
      });
    }
  });
}

//function that deletes random files
function deleteFiles(directoryPath, noOfFiles, callback) {
  //reading the directory
  fs.readdir(directoryPath, (error, files) => {
    if (error) {
      return callback(error);
    }

    let count = 1;
    //iterating through each file and deleting that file
    files.forEach((file) => {
      let filePath = path.join(directoryPath, file);
      fs.unlink(filePath, (internalError) => {
        if (internalError) {
          return callback(internalError);
        }

        // if all files are deleted,then deleting the directory
        if (count === noOfFiles) {
          // deleting the directory
          fs.rmdir(directoryPath, (directoryError) => {
            if (directoryError) {
              return callback(directoryError);
            }
            callback(null, "files deleted successfully");
          });
        }

        count += 1;
      });
    });
  });
}

//exporting the above functions
module.exports = createFilesAndDeleteFiles;
